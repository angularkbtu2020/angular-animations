import {
    animate,
    AnimationEvent,
    state,
    style,
    transition,
    trigger,
    useAnimation,
} from '@angular/animations';
import {
    Component,
    EventEmitter,
    HostBinding,
    HostListener,
    OnInit,
    Output,
} from '@angular/core';
import {
    TODO_LIST_ITEM_TRIGGER,
    HOVER_ANIMATION,
} from './todo-list-item.animation';

@Component({
    selector: 'app-todo-list-item',
    templateUrl: './todo-list-item.component.html',
    styleUrls: ['./todo-list-item.component.scss'],
    animations: [
        TODO_LIST_ITEM_TRIGGER
        // trigger('listItemHover', [
        //     state('hovered', style({ backgroundColor: 'blue' })),
        //     // transition('default => hovered', [
        //     //     useAnimation(HOVER_ANIMATION, {
        //     //         params: {
        //     //             hoverColor: 'blue',
        //     //             time: '1000ms',
        //     //         },
        //     //     }),
        //     // ]),
        //     transition('hovered => secondColor', [
        //         useAnimation(HOVER_ANIMATION, {
        //             params: {
        //                 hoverColor: 'red',
        //                 time: '1000ms',
        //             },
        //         }),
        //     ]),
        //     transition('* => hovered', [
        //         useAnimation(HOVER_ANIMATION, {
        //             params: {
        //                 hoverColor: 'blue',
        //                 time: '1000ms',
        //             },
        //         }),
        //     ]),
        //     transition(':leave', [animate(1000, style({ opacity: 0 }))]),
        // ]),
    ],
})
export class TodoListItemComponent implements OnInit {
    constructor() {}

    @Output()
    remove = new EventEmitter();

    @HostBinding('@listItemHover')
    state: 'default' | 'hovered' | 'secondColor' | 'leaving' = 'default';

    leaving = false;

    ngOnInit(): void {}

    @HostListener('mouseenter')
    mouseEnter(): void {
        if (this.leaving) {
            return;
        }
        this.state = 'hovered';
    }

    @HostListener('mouseleave')
    mouseleave(): void {
        if (this.leaving) {
            return;
        }
        this.state = 'default';
    }

    removeHandler() {
        // this.state = 'leaving';
        this.remove.emit();
        this.leaving = true;
    }

    @HostListener('@listItemHover.done', ['$event'])
    logAnimationEvents(event: AnimationEvent): void {
        if (event.toState === 'leaving') {
            this.remove.emit();
        }
        // if (event.toState === 'hovered') {
        //     this.state = 'secondColor';
        // } else if (event.toState === 'secondColor') {
        //     this.state = 'hovered';
        // }
    }
}
