import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.scss'],
    animations: [],
})
export class TodoListComponent implements OnInit {
    constructor() {}

    items: string[] = ['First', 'Second', 'Last'];

    itemControl = new FormControl('', [Validators.required]);

    addTodo(): void {
        if (this.itemControl.valid) {
            this.items.push(this.itemControl.value);
            this.itemControl.reset('');
        }
    }

    shown = false;

    remove(item: string): void {
        this.items.splice(
            this.items.findIndex((i) => i === item),
            1
        );
    }

    ngOnInit(): void {}
}
