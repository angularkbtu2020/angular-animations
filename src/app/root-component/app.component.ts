import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    animations: [
        trigger('appState', [
            state('true', style({})),
            transition(':enter', [
                style({ opacity: 0 }),
                animate('2000ms', style({ opacity: 1 })),
            ]),
        ]),
    ],
})
export class AppComponent {
    title = 'angular-animations';

    @HostBinding('@appState')
    state = true;
}
